#include <ESP8266WiFi.h>
#include "DHTesp.h"

#define USE_DISPLAY true
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 0  // GPIO0
Adafruit_SSD1306 OLED(OLED_RESET);

#define CONNECT_TIMEOUT 2065
#define SLEEP_TIME 60e6
#define PRINT true

// WiFi settings
const char* SSID     = "";
const char* PASSWORD = "";

const char* URI = "/update?api_key=";
const char* API_KEY = "";
const char* TAIL_BRIGHTNESS = "&field1=";
const char* TAIL_TEMPERATURE = "&field2=";
const char* TAIL_HUMIDITY = "&field3=";

const char* HOST = "api.thingspeak.com";
const int PORT = 80;

const int LIGHT_PIN = A0;
const int DHT_PIN = D7;
DHTesp dht;

bool serial_enabled = false;
String connection_status = "";

WiFiClient client;

void print(String string)
{
  if (PRINT)
  {
    if (!serial_enabled)
    {
      Serial.begin(115200);
      serial_enabled = true;
    }
    Serial.print(string);
  }
}

void println(String string)
{
  print(string);
  print("\n");
}

// TODO: connect to WiFi the flexible way (https://github.com/tzapu/WiFiManager)
int connectToWifi()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    return 0;
  }
  connection_status = "CONNECTING";

  // static IP, to remove long DHCP delay
  IPAddress ip(172, 20, 1, 86);
  IPAddress gateway(172, 20, 0, 0);
  IPAddress subnet(255, 255, 248, 0);
  IPAddress dns(8, 8, 8, 8);

  WiFi.config(ip, dns, gateway, subnet);
  WiFi.begin(SSID, PASSWORD);

  println("Wifi started");

  int connectAttempts = 0;
  Serial.println(WiFi.macAddress());

  //CONNECT_TIMEOUT is in seconds, we poll every .5 seconds
  while (WiFi.status() != WL_CONNECTED && connectAttempts++ < CONNECT_TIMEOUT * 2)
  {
    delay(500);
    print(".");
  }

  if (WiFi.status() == WL_CONNECTED)
  {
    connection_status = "CONNECTED";
    if (PRINT)
    {
      Serial.println(WiFi.localIP());
    }
  }

  if (PRINT && WiFi.status() != WL_CONNECTED)
  {
    connection_status = "CONNECTION FAILED";
    println(connection_status);
  }
  // return time to connect (ms. .5s precision)
  return connectAttempts * 500;
}

void setup()
{
  dht.setup(D7, DHTesp::DHT22);

  OLED.begin();
  OLED.setTextWrap(false);
  OLED.setTextSize(1);
  OLED.setTextColor(WHITE);

  connectToWifi();
}

// publish data to ThingSpeak and write to display (if enabled)
bool output(int brightness, float temperature, float humidity)
{
  bool ret = false;
#ifdef USE_DISPLAY
  OLED.clearDisplay();

  OLED.setCursor(0,0);
  OLED.print("TEMPERATURE:    ");
  OLED.println(temperature);
  OLED.print("BRIGHTNESS:     ");
  OLED.println(brightness);
  OLED.print("HUMIDITY:       ");
  OLED.println(humidity);
  OLED.println("");
  OLED.println("");
  OLED.println("");
  OLED.print("DHT: ");
  OLED.println(dht.getStatusString());
#endif

  if (WiFi.status() != WL_CONNECTED)
  {
    connection_status = "CONNECTION FAILED";
    println(connection_status);
  }
  else if (!client.connect(HOST, PORT))
  {
    connection_status = "UPLOAD FAILED";
    println(connection_status);
  }
  else
  {
    // API call to upload data
    client.print(String("GET ") + URI + API_KEY
        + TAIL_BRIGHTNESS + String(brightness)
        + TAIL_TEMPERATURE + String(temperature, 2)
        + TAIL_HUMIDITY + String(humidity, 2)
        + " HTTP/1.1\r\n"
        + "HOST: " + HOST + "\r\n"
        + "Connection: close\r\n\r\n");

    connection_status = "UPLOADED DATA";
    delay(100);
    while (client.available())
    {
      String line = client.readStringUntil('\r');
    }
    ret = true;
  }

#ifdef USE_DISPLAY
  OLED.println(connection_status);
  OLED.display();
#endif

  return ret;
}

void loop()
{
  float temp = dht.getTemperature();
  float humidity = dht.getHumidity();
  temp = dht.toFahrenheit(temp);
  output(analogRead(LIGHT_PIN), temp, humidity);
  if (PRINT)
  {
    Serial.print("BRIGHTNESS: ");
    Serial.println(analogRead(LIGHT_PIN));
    Serial.print("HUMIDITY: ");
    Serial.println(humidity);
    Serial.print("TEMPERATURE: ");
    Serial.println(temp);
  }

  //TODO: subtract time to connect to wifi
  ESP.deepSleep(SLEEP_TIME);
}
